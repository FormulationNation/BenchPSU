# Requirements

Hardware, Software and Mechanical Requirements to go here.

## Hardware Requirements

|Requirement|
|----|
|The Power supply __shall__ have the capability to operate in _Voltage Control Mode_.|
|The Power supply __shall__ have the capability to operate in _Current Control Mode_.|
|The power supply _Remote Interface_ __shall__ be isolated from the power supply output and AC mains|
|----The power Supply __should__ provide a means to interface with other equipment (i.e. _Remote Interface_).|
|The power supply __shall__ provide a Human-Machine interface on the front panel (i.e. _Local Interface_).|
|The power supply __shall__ have a LCD or similar display as a part of the _Local Interface_.|
|----The display __shall__ have a refresh rate of 10 hz or better.|
|The front panel display __shall__ display following readings in real-time: Current reading, Voltage Reading, Current setpoint, Voltage setpoint, Operating mode (CC or CV or off), remote/local status.|
|The Power supply __shall__ have a means to disconnect the output terminals from the supply output by command (5MΩ or better). This will be called _Failsafe Mode_.|
|The front panel __should__ have an accessable switch to power down the supply.|
|The power supply __should__ monitor the operating temperature.|
|The power supply __shall__ be capable of supplying 20A at 20V indefinately.|
|The power supply __shall__ operate from mains voltage 60Hz/115V.|
|The power supply output __shall__ be isolated from mains power, 1500V RMS.|
|The input of the power supply __shall__ be fused.|
|The power supply __shall__ have reverse current protection.|
|The power supply shall protect itself against ESD on the power output.|
|The chassis of the power supply __shall__ be connected to earth ground.|
|Failsafe mode __shall__ operate as the master enable, over the operational software.|
|The system __shall__ enter and latch into failsafe mode when the operational temperature exceeds 80°C.|
|The system __shall__ enter and latch into failsafe mode when the watchdog timer expires.|
|The transient response of the power supply __should__ be in the order of 100ms.|
|The power supply __shall__ be stable with a capacitive load of at most 1000uF.|
|The output ripple of the power supply __shall__ be better than 5% at full load.|
|The voltage accuracy of the power supply output __shall__ be better than 5% over the entire operational range.|
|The current accuracy of the power supply output __shall__ be better than 5% over the entire operational range.|
|The voltage accuracy of the measurement __shall__ be better than 5mV.|
|The current accuracy of the measurement __shall__ be better than 10mA.|
|The power supply __shall__ provide a hard over-voltage limit.| 
|The power supply __shall__ provide a hard over-current limit.|
|The power supply __shall__ be controlled by an onboard embedded system.|

## Software Requirements

|Requirement|
|----|
|The operational software of the power supply __shall__ compute the power output based on the voltage and current readings.|
|The operational software __shall__ reset the watchdog timer every 100ms.|

## Mechanical Requirements

|Requirement|
|----|
|The power supply __shall__ have an enclosure to protect internal electronics on all sides.|
|The enclosure __shall__ have a front panel to allow user interaction with the power supply.|
|The front panel __shall__ contain one knob for voltage control.|
|The front panel __shall__ contain one knob for current control.|
|The front panel __shall__ contain one switch to disconnect the power supply from the mains input.|
|The front panel __shall__ contain one button to enable/disable power output from the power supply.|
|The front panel __shall__ have an LED to indicate the on/off state of the power supply output.|
|The front panel __shall__ have a digital display.|
|The front panel __shall__ have three user buttons (menu, cancel, enter).|
|The enclosure __shall__ have a rear panel that contains an AC inlet, and remote interfaces.|
|The front panel __shall__ have 2 access terminals to the output power.|
